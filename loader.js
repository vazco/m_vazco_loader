'use strict';

Vazco.Loader = function Loader() {

};

Vazco.Loader.prototype.init = function init() {
    var self = this;
    _(this).each(function (component) {
        self.initHooks(component.beforeHooks);
        self.initCollections(component.collections);
        self.initMethods(component.methods);
        self.initPublications(component.publications);
        self.initRoutes(component.routes);
        self.initEvents(component.events);
        self.initHooks(component.afterHooks);
    });
};

Vazco.Loader.prototype.initCollections = function initCollections(collections) {
    if (collections) {
        _(collections).each(function (colObj, colName) {
            (Meteor.isClient ? window : global)[colName] = new Meteor.Collection(colName, colObj);
        });
    }
};

Vazco.Loader.prototype.initMethods = function initMethods(methods) {
    if (methods && Meteor.isServer) {
        Meteor.methods(methods);
    }
};

Vazco.Loader.prototype.initPublications = function initPublications(publications) {
    if (publications && Meteor.isServer) {
        _(publications).each(function (pubFunc, pubName) {
            Meteor.publish(pubName, pubFunc);
        });
    }
};

Vazco.Loader.prototype.initRoutes = function initRoutes(routes) {
    if (routes && Meteor.isClient) {
        Router.map(function () {
            var self = this;
            _(routes).each(function (routeObj, routeName) {
                self.route(routeName, routeObj);
            });
        });
    }
};

Vazco.Loader.prototype.initEvents = function initEvents(events) {
    if (events && Meteor.isClient) {
        _(events).each(function (obj, templateName) {
            if (Template[templateName]) {
                if (obj.helpers) {
                    Template[templateName].helpers(obj.helpers);
                }
                if (obj.events) {
                    Template[templateName].events(obj.events);
                }
            }
        });
    }
};

var initAllHooks = function (hooks) {
    _(hooks).each(function (hook) {
        hook();
    });
};

Vazco.Loader.prototype.initHooks = function initHooks(hooks) {
    if (hooks) {
        initAllHooks(hooks.both);
        if (Meteor.isClient) {
            initAllHooks(hooks.client);
        } else {
            initAllHooks(hooks.server);
        }
    }
};

// Make instance that components can use
Vazco.loader = new Vazco.Loader();