Package.describe({
    summary: "Vazco CMS loader package."
});

Package.on_use(function (api) {
    api.use([
        'vazco-tools-common',
    ], [
        'client',
        'server'
    ]);

    api.add_files([
        'loader.js'
    ], [
        'client',
        'server'
    ]);
});